

var express = require ('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

app.listen(port);
console.log("API escuchando en el puerto " + port);


//
app.get ('/apitechu/v1',
     function(req,res) {
       console.log("GET /apitechu/v1");
       res.send(
         {
           "msg" : "Bienvenido a la API de TechU"
         }
       )
}
);

//

app.get('/apitechu/v1/users',
  function(req,res){
     console.log("GET /apitechu/v1/users");
     res.sendFile('usuarios.json', {root: __dirname}); //enviamos usuarios json como respuesta
  }
);

//definimos metodo para crear usuarios parametros se pasan por req.headers
app.post('/apitechu/v1/users',
  function(req,res){
    console.log("POST /apitechu/v1/users");
//    console.log(req);
//de momento, los pasamos por la cabecera
    console.log ("first_name es: " + req.headers.first_name);
    console.log ("last_name es: " + req.headers.last_name);
    console.log ("country es: " + req.headers.country);

//construimos un nuevo objeto JSON
    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "country" : req.headers.country
    };

   var users = require('./usuarios.json');
   users.push(newUser);
   writeUserDataToFile(users);


   console.log("Usuario añadido con exito");



  }
);

//ejemplo de prueba de parametros

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)


// creo un nuevo post que recupera la información del body
app.post('/apitechu/v1/users/body',
  function(req,res){
    console.log("POST /apitechu/v1/users");
//    console.log(req);
//de momento, los pasamos por la cabecera
    console.log ("first_name es: " + req.body.first_name);
    console.log ("last_name es: " + req.body.last_name);
    console.log ("country es: " + req.body.country);

//construimos un nuevo objeto JSON
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

   var users = require('./usuarios.json');
   users.push(newUser);
   writeUserDataToFile(users);


   console.log("Usuario añadido con exito");



  }
);



//incorporamos funcion para borrar incluyendo parameteos en la URL
app.delete('/apitechu/v1/users/:id',
  function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    writeUserDataToFile(users);

    res.send(
      {
        "msg" : "Usuario borrado con exito"
      }
    )
  }
);


// función escritura de ficheros, hemos creado la funcion de
// escritura para factorizar la escritura de ficheros

function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  console.log ("Escribo fichero");

  fs.writeFile(
    "./login.json",
    jsonUserData,
    "utf8",
    function(err) {
      if(err) {
        console.log(err);
      } else {
        console.log("Usuario persistido");
      }
   }
 )
}


// LOGIN post para recuperar datos de LOGIN

app.post('/apitechu/v1/login',
  function(req,res){
    console.log("POST /apitechu/v1/login");
//    console.log(req);
//de momento, los pasamos por la cabecera


//construimos un nuevo objeto JSON

   var users = require('./login.json');


   for (user of users) {
      console.log (user.email);
      console.log (user.password);
      console.log("Paso por bucle");
    if (req.body.email == user.email) {
      if (req.body.password == user.password){
        console.log (user.email);
        console.log (user.password);
        console.log ("usuario encontrado");
        console.log ("Paso por la funcion");
        user.logged = true;

        writeUserDataToFile(users);

        res.send (
          {
            "msg": "Login correcto",
            "id" : user.id
          }
        )
      }
      break;
    }

}
res.send (
  {
    "msg" : "login incorrecto"
  }
)
console.log ("usuario no encontrado");
}
);

app.post('/apitechu/v1/logout',
  function(req,res){
    console.log("POST /apitechu/v1/logout");
//    console.log(req);
//de momento, los pasamos por la cabecera


//construimos un nuevo objeto JSON

   var users = require('./login.json');

   for (user of users) {

      console.log("Paso por bucle");
    if (req.body.id == user.id) {
      if (user.logged){
        console.log ("usuario encontrado, listo para borrar")
        delete user.logged;
        writeUserDataToFile(users);







        res.send(
          {
            "msg" : "Logout satisfactorio",
            "id" : user.id
          }
        )
      }
      break;
    }


   }

   res.send (
     {
       "msg" : "logout incorrecto"
     }
)


  }
);
